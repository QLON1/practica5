package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorCliente {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Pastel> listaPasteles;
	
	public GestorCliente() {
        listaClientes = new ArrayList<Cliente>();
        listaPasteles = new ArrayList<Pastel>();
    }
	
	 public void altaCliente (String nombre, String apellido, String genero, int telefono, String dni) {
	
		  if (!existeCliente(dni)) {
			 Cliente nuevoCliente = new Cliente (nombre, dni);
			 nuevoCliente.setApellido(apellido);
			 nuevoCliente.setGenero(genero);
			 nuevoCliente.setTelefono(telefono);
			 listaClientes.add(nuevoCliente);
		 }
		 else {
			 System.out.println("El Cliente ya Existe");
		 }
	 }
	
	 public boolean existeCliente(String dni) {
		 for (Cliente cliente : listaClientes) {
			 if (cliente != null & cliente.getDni().equals(dni)) {
				 return true;
			 }
		 }
		 
		 return false;
	 }
	 
 public void listarCliente() {
		 
		 
		 for (int i =0; i<listaClientes.size(); i++) {
			 Cliente cliente = listaClientes.get(i);
			 if (cliente != null) {
				 System.out.println(cliente);
			 }
		 } 
	 }
 
 public Cliente buscarCliente(String dni) {
		for (Cliente cliente1 : listaClientes) {
			if (cliente1 != null && cliente1.getDni().equals(dni)) {
				return cliente1;
			}
		}
		return null;
	}
 public void eliminarPaciente(String dni) {
     Iterator<Cliente> iteratorcliente = listaClientes.listIterator();


     while(iteratorcliente.hasNext()) {
         Cliente cliente = iteratorcliente.next();
         if (cliente.getDni().equals(dni)) {
             iteratorcliente.remove();
         }
     }

 }
 
 public void altaPastel(String nombreTarta,String sabor,  double precio, int cantidadtartas, String fechaCompra, String codPastel) {
		
		if (!existePastel(codPastel)) {
			Pastel nuevoPastel = new Pastel(codPastel);
			nuevoPastel.setNombreTarta(nombreTarta);
			nuevoPastel.setSabor(sabor);
			nuevoPastel.setPrecio(precio);
			nuevoPastel.setCantidatartas(cantidadtartas);
			nuevoPastel.setFechaCompra(LocalDate.now());
			listaPasteles.add(nuevoPastel);
		}
		
	
		System.out.println("El pastel ya esta vendido");
	}
 
 public boolean existePastel(String codPastel) {
		for (Pastel pastel :listaPasteles) {
			if (pastel != null && pastel.getCodPastel().equals(codPastel)) {
				return true;
			
				}
			}
		return false;
	}
 
 public void listarPasteles() {
		
		for (int i =0; i<listaPasteles.size(); i++) {
			 Pastel pastel = listaPasteles.get(i);
			 if (pastel != null) {
				 System.out.println(pastel);
			 }
		 } 
		
	}
 public void eliminarPastel(String codPastel) {
		
		Iterator<Pastel> iteratorPastel = listaPasteles.listIterator();
	
	while (iteratorPastel.hasNext()) {
		Pastel pastel = iteratorPastel.next();
			if (pastel.getCodPastel().equals(codPastel)) {
				iteratorPastel.remove();
			}		
		}	
	}
 
 
 public Pastel busquedaPastel(String codPastel) {
		
		for (int i =0; i<listaPasteles.size(); i++) {
			Pastel pastel= listaPasteles.get(i);
			if ( pastel != null && pastel.getCodPastel().equals(codPastel)) {
				System.out.println(pastel);
				return pastel;
			}
		}
		System.out.println("El pastel solicitado  no existe");
		return null;
	}
 
 public void listarPastelPorPrecio(double precio) {
		
		for (Pastel pastel : listaPasteles) {
			if (pastel.getPrecio()==precio){
				System.out.println(pastel);
			}
		}
	}
 
 public void asignarCliente(String dni,  String codPastel ) {
		
		if (buscarCliente(dni) != null && busquedaPastel(codPastel)!=null) {
			Cliente cliente = buscarCliente(dni);
			System.out.println(cliente);
			Pastel pastel = busquedaPastel(codPastel);
			System.out.println(pastel);
			pastel.setCompradorPastel(cliente);
		}

	}
 
 public void listarClientePastel(String dni) {
		for (Pastel pastel : listaPasteles) {
			if (pastel.getCompradorPastel()!=null && pastel.getCompradorPastel().getDni().equals(dni)) {
				System.out.println(pastel);
			}
		}	
	}

	
	public void cambiarNombreCliente(String nombre, String nombre2) {
		for (int i = 0; i < listaClientes.size(); i++) {
			Cliente cliente = listaClientes.get(i);
			if (cliente != null) {
				if (cliente.getNombre().equals(nombre)) {
					cliente.setNombre(nombre2);
				}
			}
		}
	}
	
	public void cambiarCodigoPastel(String codPastel, String codPastel2) {
		for (int i = 0; i < listaPasteles.size(); i++) {
			Pastel pastel = listaPasteles.get(i);
			if (pastel != null) {
				if (pastel.getCodPastel().equals(codPastel)) {
					pastel.setCodPastel(codPastel2);
				}
			}
		}
	}
	
	public void mediaPastel() {
		double total =0 ;
		for (Pastel pastel : listaPasteles) {
			if (pastel != null) {
				total = total + pastel.getPrecio();
			}
		}
		
		System.out.println(total/listaPasteles
				.size());
	}
 
	public void maxPastel() {
		double max =0;
		for (Pastel pastel : listaPasteles) {
			if (pastel != null) {
				if(max<pastel.getPrecio()) {
					max = pastel.getPrecio();
				}
			}
			
		}
		System.out.println("El precio maximo es " + max);
		
	}
	
}

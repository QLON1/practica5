package clases;

import java.time.LocalDate;

public class Pastel {
	private	String nombreTarta;
	private	String sabor;
	private	double precio;
	private	int cantidatartas;
	private Cliente compradorPastel;
	private LocalDate fechaCompra;
	private String codPastel;
	public Pastel(String codPastel) {
		this.codPastel = codPastel;
	}
	public String getNombreTarta() {
		return nombreTarta;
	}
	public void setNombreTarta(String nombreTarta) {
		this.nombreTarta = nombreTarta;
	}
	public String getSabor() {
		return sabor;
	}
	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getCantidatartas() {
		return cantidatartas;
	}
	public void setCantidatartas(int cantidatartas) {
		this.cantidatartas = cantidatartas;
	}
	public Cliente getCompradorPastel() {
		return compradorPastel;
	}
	public void setCompradorPastel(Cliente compradorPastel) {
		this.compradorPastel = compradorPastel;
	}
	public LocalDate getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public String getCodPastel() {
		return codPastel;
	}
	public void setCodPastel(String codPastel) {
		this.codPastel = codPastel;
	}
	@Override
	public String toString() {
		return "Pastel [nombreTarta=" + nombreTarta + ", sabor=" + sabor + ", precio=" + precio + ", cantidatartas="
				+ cantidatartas + ", compradorPastel=" + compradorPastel + ", fechaCompra=" + fechaCompra
				+ ", codPastel=" + codPastel + "]";
	}
	
	
	
}

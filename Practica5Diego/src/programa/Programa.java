package programa;

import java.util.Scanner;

import clases.GestorCliente;

public class Programa {

	public static void main(String[] args) {
		GestorCliente gestor = new GestorCliente();
		Scanner escaner = new Scanner(System.in);
		int opcion;
		do {
			System.out.println("1-Dar de alta un cliente");
			System.out.println("2-Listar los cliente");
			System.out.println("3-Buscar cliente");
			System.out.println("4-Eliminar cliente");
			System.out.println("5-Dar de alta un pastel");
			System.out.println("6-Listar los pasteles");
			System.out.println("7-Buscar pastel");
			System.out.println("8-Eliminar pastel");
			System.out.println("9-Listar pasteles por precio");
			System.out.println("10-Listar lpasteles por clientes");
			System.out.println("11-editar codigo Pastel");
			System.out.println("12-editar nombre cliente");
			System.out.println("13-precio maximo");
			System.out.println("14- La media del precio de  los pasteles");
			System.out.println("15- Asignar un cliente a un pastel");
			System.out.println("16-salir");
			opcion = escaner.nextInt();
			escaner.nextLine();
			switch (opcion) {
			case 1:
				System.out.println("introduce un nombre");
				String nombre = escaner.nextLine();
				System.out.println("introduce un apellido");
				String apellido = escaner.nextLine();
				System.out.println("introduce un genero");
				String genero = escaner.nextLine();
				System.out.println("introduce un telefono");
				int telefono = escaner.nextInt();
				escaner.nextLine();
				System.out.println("introduce un dni");
				String dni = escaner.nextLine();
				gestor.altaCliente(nombre, apellido, genero, telefono, dni);
				gestor.listarCliente();
				break;
				
				
			case 2:
				gestor.listarCliente();
				break;
			case 3:
				System.out.println("dime un dni que quieras buscar");
				String nombreCliente =escaner.next();
				System.out.println(gestor.buscarCliente(nombreCliente));	
				
				break;
				
			case 4:
				System.out.println("dame un dni de un cliente para eliminar");
				String dniCliente = escaner.next();
				gestor.eliminarPaciente(dniCliente);;
				gestor.listarCliente();
				
				
				break;
			case 5:
				System.out.println("nombre pastel");
				String nombreTarta =escaner.nextLine();
				System.out.println("sabor");
				String sabor = escaner.nextLine();
				System.out.println("precio");
				double precio = escaner.nextDouble();
				escaner.nextLine();
				System.out.println("cantidad tarta");
				int cantidadTartas = escaner.nextInt();
				System.out.println("fechaCompra");
				String fechaCompra = escaner.nextLine();
				System.out.println("hoy");
				System.out.println("codPastel");
				String codPastel = escaner.nextLine();
				gestor.altaPastel(nombreTarta, sabor, precio, cantidadTartas, fechaCompra, codPastel);
				gestor.listarPasteles();
				break;
				
			case 6:
				gestor.listarPasteles();
				
				break;
				
			case 7:
				System.out.println("dime el codigo del pastel que quieres buscar");
				String codPastelbuscado = escaner.nextLine();
				gestor.busquedaPastel(codPastelbuscado);
				break;
				
			case 8:
				System.out.println("introduce el cod pastel a eliminar ");
				String codigoeliminado = escaner.nextLine();
				gestor.eliminarPastel(codigoeliminado);
				break;
				
			case 9:
				System.out.println("introduce el precio del pastel para filtrar");
				double precioPastel = escaner.nextDouble();
				gestor.listarPastelPorPrecio(precioPastel);
				break;
				
			case 10:
				System.out.println("introduce el dni del cliente");
				String dnicliente = escaner.nextLine();
				gestor.listarClientePastel(dnicliente);
				break;
				
			case 11:
				System.out.println("introduce el codigo del pastel que quieres cambiar");
				String cambiocodigo= escaner.nextLine();
				System.out.println("dame el nuevo codigo");
				String codNuevo = escaner.nextLine();
				gestor.cambiarCodigoPastel(cambiocodigo, codNuevo);
				gestor.listarPasteles();
				break;
				
			case 12:
				System.out.println("dime el  nombre del cliente que quieres cambiar");
				String Nombrecambio = escaner.nextLine();
				System.out.println("dime el nuevo nombre");
				String Nombrenuevo = escaner.nextLine();
				gestor.cambiarNombreCliente(Nombrecambio, Nombrenuevo);
				gestor.listarCliente();
				
				break;
				
			case 13:
				gestor.maxPastel();
				break;
				
			case 14:
				gestor.mediaPastel();
				break;
			case 15:
				System.out.println("introduce el dni");
				String dniasignado = escaner.nextLine();
				System.out.println("Introduce el codigo del pastel:");
				String codPastelasignado = escaner.nextLine();
				gestor.asignarCliente(dniasignado, codPastelasignado);
				
				break;

			case 16:
				System.out.println("salir ");
				break;
			
				
			}
		

	}while(opcion!=16);
		
		escaner.close();
	}

}
